/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author natta
 */
public class ProductService {
    private final ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" product_name ASC");
    }

    public Object getProductOrderByName() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
